# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Courses(models.Model):
    _name = 'open_academy.courses'
    _description = 'courses'
    name = fields.Char(string='Nombre', required=True)
    responsible = fields.Many2one(string='Responsable', comodel_name='res.users')
    description = fields.Char(string='Descripcion', required=True)
    session = fields.One2many('open_academy.session', 'course', 'Sesion')


    class Session(models.Model):
        _name = 'open_academy.session'
        _descriptiion = 'session'
        name = fields.Char(string='Nombre', required=True)
        instructor = fields.Many2one(string='Instructor', comodel_name='res.partner')
        course = fields.Many2one(string='Curso', comodel_name='open_academy.courses')
        startdate = fields.Datetime(string='Fecha Inicio')
        duration = fields.Char(string='Duración')
        numberseats =  fields.Char(string='Numero Asistentes')
        attendees = fields.Many2many(string='Asistentes', comodel_name='res.partner')

    class instructor(models.Model):
        _inherit = 'res.partner'
        active = fields.Boolean(string='Activo')


